import React, {Component} from 'react';
import Loto from "./components/Loto/Loto"

class App extends Component {
  getNumbers = () => {
    let numbers = [];
    let max = 5;
    for (let i = 0; i < max; i++) {
      let a = Math.round(Math.random() * (36 - 5) + 5);
      if (a !== numbers[0] & a !== numbers[1] & a !== numbers[2] & a !== numbers[3] & a !== numbers[4]) {
        numbers.push(a);
      } else {
        max++;
      }
    }
    numbers.sort(function (a, b) {
      return a - b;
    });

    return numbers;
  };

  state = {
    numbers: this.getNumbers()
  };

  changeName = () => {

    const numbersNew = this.getNumbers(); 
    this.setState({numbers: numbersNew});
  };

  render() {
    return (
        <div className="App">
          <Loto number1={this.state.numbers[0]} number2={this.state.numbers[1]} number3={this.state.numbers[2]} number4={this.state.numbers[3]} number5={this.state.numbers[4]}>
            <button className="btn" onClick={this.changeName}>Change numbers</button>
          </Loto>
        </div>
    );
  }
}

export default App;
