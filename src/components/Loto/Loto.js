import React, {Component} from "react";

import "./Loto.css";

class Loto extends Component {
    render() {
        return (
            <div>
                <p>{this.props.children}</p>
                <div>
                    <span className="number">{this.props.number1}</span>
                    <span className="number">{this.props.number2}</span>
                    <span className="number">{this.props.number3}</span>
                    <span className="number">{this.props.number4}</span>
                    <span className="number">{this.props.number5}</span>
                </div>
            </div>
        );
    }
}

export default Loto;
